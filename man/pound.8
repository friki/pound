.TH POUND "8" "Jan 2010" "pound" "System Manager's Manual"
.SH NAME
pound \- HTTP/HTTPS reverse-proxy and load-balancer
.SH SYNOPSIS
.TP
.B pound
[\fI-v\fR]
[\fI-c\fR]
[\fI-d level\fR]
[\fI-f config_file\fR]
[\fI-p pid_file\fR]
.SH DESCRIPTION
.PP
.B Pound
is a reverse-proxy load balancing server. It accepts requests from HTTP/HTTPS
clients and distributes them to one or more Web servers. The HTTPS requests are
decrypted and passed to the back-ends as plain HTTP.
.PP
If more than one back-end server is defined,
.B Pound
chooses one of them randomly. By default,
.B Pound
keeps track of associations between clients and back-end servers (sessions).
.SH GENERAL PRINCIPLES
.P
In general
.B Pound
needs three types of objects defined in order to function:
.IR listeners ,
.I services
and
.IR back-ends .
.TP
\fBListeners\fR
A
.I listener
is a definition of how
.B Pound
receives requests from the clients (browsers). Two types of
.I listeners
may be defined: regular HTTP
.I listeners
and HTTPS (HTTP over SSL/TLS)
.IR listeners .
At the very least a
.I listener
must define the address and port to listen on, with additional
requirements for HTTPS
.IR listeners .
.TP
\fBServices\fR
A
.I service
is the definition of how the requests are answered. When a request is received
.B Pound
attempts to match them to each
.I service
in turn. The
.I services
may define their own conditions as to which requests they can answer:
typically this involves certain URLs (images only, or a certain path)
or specific headers (such as the Host header).
.TP
\fBBack-ends\fR
The
.I back-ends
are the actual servers for the content requested. By itself,
.B Pound
supplies no responses - all contents must be received from a "real"
web server. The
.I back-end
defines how the server should be contacted.
.IP
Multiple
.I back-ends
may be used within a
.IR service ,
in which case
.B Pound
will load-balance between the available
.IR back-ends .
.IP
If a
.I back-end
fails to respond it will be considered "dead", in which case
.B Pound
will stop sending requests to it. Dead
.I back-ends
are periodically checked for availability, and once they respond again they
are "resurected" and requests are sent again their way. If no
.I back-ends
are available (none were defined, or all are "dead") then
.B Pound
will reply with "503 Service Unavailable", without checking additional
.IR services .
.IP
The connection between
.B Pound
and the
.I back-ends
is always via HTTP, regardless of the actual protocol used between
.B Pound
and the client.
.SH OPTIONS
Options available (see also below for configuration file options):
.TP
\fB\-v\fR
Print version:
.B Pound
will exit immediately after printing the current version.
.TP
\fB\-c\fR
Check only:
.B Pound
will exit immediately after parsing the configuration file. This may be used for
running a quick syntax check before actually activating a server.
.TP
\fB\-d\fR level
Debug mode: if level is greater than 0 error messages will be sent to stdout and
.B Pound
will stay in the foreground. Level 0 (default) are the regular log messages, level 1 and up will produce more detailed information.
.TP
\fB\-f\fR config_file
Location of the configuration file (see below for a full description of the format).
Default:
.I /etc/pound/pound.yaml
.TP
\fB\-p\fR pid_file
Location of the pid file.
.B Pound
will write its own pid into this file. Normally this is used for shell
scripts that control starting and stopping of the daemon.
Default:
.I /var/run/pound.pid
.PP
One (or more) copies of
.B Pound
should be started at boot time. Use "big iron" if you expect heavy loads: while
.B Pound
is as light-weight as we know how to make it, with a lot of simultaneous requests it
will use quite a bit of CPU and memory. Multiple CPUs are your friend.
.SH "CONFIGURATION FILE"
The configuration file is in standard YAML syntax. There are four
blocks of directives:
.B Global
directives (they affect the settings for the entire program instance),
.B Backends
directives, defining the available backends,
.B HTTPlisteners
directives (they define which requests
.B Pound
will listen for), and
.B HTTPSlisteners
directives (same as HTTPlistener but via TLS).
.SS "Global Directives"
.TP
\fBUser\fR: user_name
Specify the user
.B Pound
will run as (must be defined in \fI/etc/passwd\fR).
.TP
\fBGroup\fR: group_name
Specify the group
.B Pound
will run as (must be defined in \fI/etc/group\fR).
.TP
\fBRootJail\fR: directory_path_and_name
Specify the directory that
.B Pound
will chroot to at runtime. Please note that SSL may require access to /dev/urandom,
so make sure you create a device by that name, accessible from the root jail
directory.
.B Pound
may also require access to
.I /dev/syslog
or similar.
.TP
\fBErr404\fR: path_to_file
Specify a path to an HTML file to be returned in case of a 404 error.
.TP
\fBErr405\fR: path_to_file
Specify a path to an HTML file to be returned in case of a 405 error.
.TP
\fBErr500\fR: path_to_file
Specify a path to an HTML file to be returned in case of a 500 error.
.SS "Backends"
A back-end is a definition of a single back-end server
.B Pound
will use to reply to incoming requests. Each backend must be marked with an
anchor. The following directives are available:
.TP
\fBAddress\fR: address
The address that
.B Pound
will connect to. This can be a numeric IP address, or a symbolic host name
that must be resolvable at run-time. This is a
.B mandatory
parameter.
.TP
\fBPort\fR: port
The port number that
.B Pound
will connect to. This is a
.B mandatory
parameter.
.TP
\fBTimeout\fR: number
How long to wait for a backend (server) to complete and operation. Default: 15 seconds.
.TP
\fBThreads\fR: number
How many threads will be used to service requests to this backend. See also below for
remarks on performance tuning. Default: 8 threads.
.TP
\fBHeadAdd\fR: header
A header to add to each reply received from this backend. The header is a string.
.SS "HTTPListeners"
An HTTP listener defines an address and port that
.B Pound
will listen on for HTTP requests. The following directives are
available:
.TP
\fBAddress\fR: address
The address that
.B Pound
will listen on. This can be a numeric IP address, or a symbolic host name
that must be resolvable at run-time.  This is a
.B mandatory
parameter. The address 0.0.0.0 may be used as an alias for 'all available
addresses on this machine', but this practice is strongly discouraged.
.TP
\fBPort\fR: port
The port number that
.B Pound
will listen on.  This is a
.B mandatory
parameter.
.TP
\fBClient\fR: value
Define how long
.B Pound
will wait for client activity. Default: 5 seconds.
.TP
\fBThreads\fR: value
Define how many threads
.B Pound
will use to service client requests. Default: 8 threads.
.TP
\fBServices\fR:
This defines a service. This service will be used only by this listener.
.SS "Services"
The following directives are allowed in a service definition:
.TP
\fBURL\fR: pattern
The service will only be used if the request URL matches the given pattern.
.TP
\fBHeadRequire\fR: pattern
Use the service only if
.I any
of the request headers matches the given pattern.
.TP
\fBHeadDeny\fR: pattern
Use the service only if
.I none
of the request headers matches the given pattern.
.TP
\fBSession\fR: number
How long to keep the client sessions (in seconds). Sessions are a long term association
between a client IP address and a specific backend in this service. A value of 0 seconds
means no sessions are kept. Default: 0.
.TP
\fBBackEnds\fR:
A list of references to previously defined backends.
.SS "HTTPSListeners"
All
.I HTTPListeners
directives are also available in the
.I HTTPSListener
blocks.
 The following additional directives
are available:
.TP
\fBCertificates\fR:
A list of file names. Each file must contain a certificate, optionally additional chained
certificates up to a known certificate authority, and the private key corresponding to the
certificate.
\fINote\fR:
the private key should probably not be password-protected, as
.B Pound
normally starts as a daemon and cannot ask for the password at start-up time.
.TP
\fBCiphers\fR:
A list of acceptable cipher names for this listener. The negotiation with the client will
result in one of these ciphers being used, or the hand-shake will fail.
.SH "ADDITIONAL REMARKS"
.SS "High-availability"
.B Pound
attempts to keep track of active back-end servers, and will temporarily disable
servers that do not respond (though not necessarily dead: an overloaded server
that
.B Pound
cannot establish a connection to will be considered dead). However, every 60
seconds (compile-time option), an attempt is made to connect to the dead servers in case
they have become active again. If this attempt succeeds, connections will be initiated to
them again.
.PP
The clients that happen upon a dead backend server will just receive a
.I "503 Service Unavailable"
message.
.SH Security
.PP
In general,
.B Pound
does not read or write to the hard-disk. The exceptions are reading the configuration file
and (possibly) the server certificate file(s) and error message(s), which are opened read-only
on startup, read,
and closed; secondly the pid file which is opened on start-up, written to and immediately closed.
Following this there is no disk access whatsoever, so using a RootJail directive is only
for extra security bonus points.
.PP
.B Pound
tries to sanitise all HTTP/HTTPS requests: the request itself, the headers and the contents
are checked for conformance to the RFC's and only valid requests are passed to the back-end
servers. This is not absolutely fool-proof - as the recent Apache problem with chunked
transfers demonstrated. However, given the current standards, this is the best that can
be done - HTTP is an inherently weak protocol.
.SS "Additional Notes"
.B Pound
uses the system log for messages (default facility LOG_DAEMON - compile-time option).
The format is very similar to other web servers, so if you want to use a log tool:
.RS
.EX
    fgrep pound /var/log/messages | cut -d ':' -f 4- | your_log_tool
.EE
.RE
(assuming
.I messages
is you log file; it may be
.I syslog
or something else, depending on your configuration).
.PP
.B Pound
deals with (and sanitizes) HTTP/1.1 requests. Thus a single connection to an HTTP/1.1 client
is kept, while the connection to the back-end server is (re)opened as necessary.
.PP
Unless you start
.B Pound
as root it won't be able to listen on privileged ports. That applies even if you do start it
as root but set the
.I User
to something else.
.PP
There is no point in setting
.I User
to root: either you start as root, so you already are, or you are not allowed to setuid(0).
.SS "Performance Tuning Considerations"
The two important factors in tuning the performance are the number of threads for the backends
end the number of threads for the listeners.
.PP
The number of backend threads defines how many requests may be issued in parallel to a specific
backend server, but also backend priorities. Increasing it may overload the web server, but
setting it too low will cause longer wating ques for servicing requests. Please note that
you may define several backends for the same server in order to use them in separate services.
.PP
The number of listener threads defines how many client requests can be serviced in
parallel. If this number is too low for your load clients may be faced with long waiting times
even when the backends are almost idle.
.SH "EXAMPLES"
The simplest configuration, with
.I Pound
used strictly to sanitise requests:
.RS
.EX
Backends:
    - &be
        Address: 10.1.1.100
        Port: 80

HTTPListeners:
    -   Address: 123.1.2.3
        Port: 80
        Services:
            -   Backends:
                    - *be

HTTPSListeners:
.EE
.RE
.PP
The same thing, but with HTTPS:
.RS
.EX
Backends:
    - &be
        Address: 10.1.1.100
        Port: 80

HTTPListeners:

HTTPSListeners:
    -   Address: 123.1.2.3
        Port: 443
        Services:
            -   Backends:
                    - *be
        Certificates:
            - "cert.pem"
        Client: 60
        Ciphers:
            - TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384
            - TLS-DHE-RSA-WITH-3DES-EDE-CBC-SHA
            - TLS-DHE-RSA-WITH-AES-128-CBC-SHA
            - TLS-RSA-WITH-CAMELLIA-128-CBC-SHA
            - TLS-RSA-WITH-AES-128-CCM
            - TLS-RSA-WITH-AES-256-GCM-SHA384
            - TLS-RSA-WITH-RC4-128-MD5
            - TLS-RSA-WITH-3DES-EDE-CBC-SHA
.EE
.RE
.PP
To distribute the HTTP/HTTPS requests to three Web servers, where the third one
is a newer and faster machine:
.RS
.EX
Backends:
    - &be0
        Address: 10.1.1.100
        Port: 80
        Threads: 8
    - &be1
        Address: 10.1.1.101
        Port: 80
        Threads: 8
    - &be2
        Address: 10.1.1.102
        Port: 80
        Threads: 12

HTTPListeners:
    -   Address: 123.1.2.3
        Port: 80
        Threads: 32
        Services:
            -   Backends:
                    - *be0
                    - *be1
                    - *be2

HTTPSListeners:
.EE
.RE
.PP
To separate between image requests and other Web content:
.RS
.EX
Backends:
    - &text
        Address: 10.1.1.100
        Port: 80
        Threads: 16
    - &images
        Address: 10.1.1.101
        Port: 80
        Threads: 16

HTTPListeners:
    -   Address: 123.1.2.3
        Port: 80
        Threads: 32
        Services:
            -   URL: ".*\.(gif|jpg|png)"
                Backends:
                    - *images
            -   Session: 300
                Backends:
                    - *text

HTTPSListeners:
.EE
.RE
.SH FILES
.TP
\fI/var/run/pound.pid\fR
this is where
.B Pound
will attempt to record its process id.
.TP
\fI/etc/pound/pound.yaml\fR
the default configuration file (compile-time option).
.SH "AUTHOR"
Written by Robert Segall, Apsis GmbH.
.SH "REPORTING BUGS"
Report bugs to <roseg@apsis.ch>.
.SH "COPYRIGHT"
Copyright \(co 2002-2020 Apsis GmbH.
.br
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.